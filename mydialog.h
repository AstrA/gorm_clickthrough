/****************************************************************************
**
** Created by Attila Gecse
** Contact: attila.gecse@mail.com
**
** Created at : 2016.12.16. by AstrA
**
**
****************************************************************************/
#ifndef MYDIALOG_H
#define MYDIALOG_H

#include <QDialog>

namespace Ui {
class MyDialog;
}

class MyDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MyDialog(QWidget *parent = 0);
    ~MyDialog();

protected:
    void changeEvent(QEvent *e);

private slots:
    void on_pushButton_ShowClickThrough_clicked();

private:
    Ui::MyDialog *ui;
};

#endif // MYDIALOG_H
