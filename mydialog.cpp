/****************************************************************************
**
** Created by Attila Gecse
** Contact: attila.gecse@mail.com
**
** Created at : 2016.12.16. by AstrA
**
**
****************************************************************************/
#include "mydialog.h"
#include "ui_mydialog.h"

#include "notificationldialog.h"

#include <QTimer>

MyDialog::MyDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MyDialog)
{
    ui->setupUi(this);
}

MyDialog::~MyDialog()
{
    delete ui;
}

void MyDialog::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void MyDialog::on_pushButton_ShowClickThrough_clicked()
{
    auto dialog = new NotificationlDialog(this);

    dialog->show();

    QTimer::singleShot(5000, [dialog](){
        dialog->hide();
    });
}
