#-------------------------------------------------
#
# Project created by QtCreator 2016-12-16T19:44:50
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++14
TARGET = gorm_clickthrough
TEMPLATE = app


SOURCES += main.cpp\
        mydialog.cpp \
    notificationldialog.cpp

HEADERS  += mydialog.h \
    notificationldialog.h

FORMS    += mydialog.ui \
    notificationldialog.ui
