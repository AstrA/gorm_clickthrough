/****************************************************************************
**
** Created by Attila Gecse
** Contact: attila.gecse@mail.com
**
** Created at : 2016.12.16. by AstrA
**
**
****************************************************************************/
#include "notificationldialog.h"
#include "ui_notificationldialog.h"

#include <QImage>
#include <QPainter>

NotificationlDialog::NotificationlDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NotificationlDialog)
{
    ui->setupUi(this);

    setWindowOpacity(9.0);
    setAttribute(Qt::WA_TranslucentBackground);
    setAttribute(Qt::WA_TransparentForMouseEvents);
    setWindowFlags(Qt::FramelessWindowHint | Qt::Tool | Qt::WindowStaysOnTopHint);

    _image = new QImage(size(), QImage::Format_ARGB32);
    _image->fill(qRgba(74, 74, 74, 210));

    for (int x = 0 ; x < size().width() ; ++x)
    {
        for (int y = 0 ; y < size().height() ; ++y)
        {
            if (x == 0 || y == 0)
            {
                _image->setPixel(x, y, qRgba(200, 200, 200, 210));
            }
            else if (x == size().width() - 1 || y < size().height() - 1)
            {
                _image->setPixel(x, y, qRgba(50, 50, 50, 210));
            }
        }
    }
}

NotificationlDialog::~NotificationlDialog()
{
    delete ui;
    delete _image;
}

void NotificationlDialog::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void NotificationlDialog::paintEvent(QPaintEvent* event)
{
    QPainter painter(this);

    painter.drawImage(rect(), *_image);

    QDialog::paintEvent(event);
}
