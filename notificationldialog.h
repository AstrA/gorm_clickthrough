/****************************************************************************
**
** Created by Attila Gecse
** Contact: attila.gecse@mail.com
**
** Created at : 2016.12.16. by AstrA
**
**
****************************************************************************/
#ifndef NOTIFICATIONLDIALOG_H
#define NOTIFICATIONLDIALOG_H

#include <QDialog>

class QImage;

namespace Ui {
class NotificationlDialog;
}

class NotificationlDialog : public QDialog
{
    Q_OBJECT

public:
    explicit NotificationlDialog(QWidget *parent = 0);
    ~NotificationlDialog();

protected:
    void changeEvent(QEvent *e);
    virtual void paintEvent(QPaintEvent* event) override;

private:
    Ui::NotificationlDialog *ui;
    QImage* _image = nullptr;

};

#endif // NOTIFICATIONLDIALOG_H
