/****************************************************************************
**
** Created by Attila Gecse
** Contact: attila.gecse@mail.com
**
** Created at : 2016.12.16. by AstrA
**
**
****************************************************************************/
#include "mydialog.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MyDialog w;
    w.show();

    return a.exec();
}
